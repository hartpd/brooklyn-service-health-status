import { BrooklynWebHealthStatusPage } from './app.po';

describe('brooklyn-web-health-status App', function() {
  let page: BrooklynWebHealthStatusPage;

  beforeEach(() => {
    page = new BrooklynWebHealthStatusPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
