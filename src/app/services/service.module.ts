import { NgModule } from '@angular/core';
import { RouterModule} from '@angular/router';

import { ServiceListComponent } from './service-list.component';
import { ServiceService } from './service.service';

@NgModule({
    //declarations: [ServiceListComponent],
    imports: [
        RouterModule.forChild([
            { path: 'services', component: ServiceListComponent },
        ])
    ],
    providers: [ServiceService]
})
export class ServiceModule {}
